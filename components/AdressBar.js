import React, { Component } from 'react'
import { Text, View ,TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import MyContext from './MyContext'

export default class AdressBar extends Component {
    static contextType = MyContext;

    render() {
        return (
            <View style={{
                paddingVertical:10,
                backgroundColor:'#0B2030',
                borderBottomRightRadius:20,
                borderBottomLeftRadius:20,
                alignItems:'center',justifyContent:'center'}}>
                <Text style={{
                    justifyContent:'flex-start',
                    marginBottom:10,
                    paddingHorizontal:15,
                    paddingVertical:5,
                    borderRadius:5,
                    letterSpacing:1,
                    color:'white',
                    // backgroundColor:'#959595',
                    textTransform:'uppercase'
                    }}>Адресс доставки</Text>
                <TouchableOpacity onPress={()=> this.props.navigation.navigate('Map')}
                style={{
                    borderRadius:50,
                    backgroundColor:'white',
                    height:50,
                    width:'98%',
                    paddingLeft:20,
                    elevation: 10,
                    shadowColor: '#F0F5F8',
                    shadowOffset: {
                        width: 5,
                        height: 10
                    },
                    shadowRadius: 50,
                    shadowOpacity: 0.5,                    
                    position:'relative',
                    flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>
                    <Text style={{letterSpacing:1}}>{this.context.cart.address}</Text>
                    <View style={{position:'absolute',right:10, 
                    borderRadius: 20,
                    elevation: 10,
                    shadowColor: '#000000',
                    shadowOffset: {
                        width: 5,
                        height: 10
                    },
                    shadowRadius: 50,
                    shadowOpacity: 0.5,                        
                    }}>
                        <Icon name="compass" size={24} />                            
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}
