import React, { Component } from 'react'
import { Text, View , Image , TouchableOpacity  } from 'react-native'

export default class Food extends Component {
    render() {
        return (
            <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Food',
            {
            alias: this.props.alias,
            id: this.props.id
            })}
            style={{
            flex:1,backgroundColor:'white',
            marginBottom:20,
            borderRadius:10,
            marginHorizontal:10,
            paddingBottom: 25,
            paddingHorizontal: 10,
            alignItems:'center',
            textAlign:'center',
            shadowColor: "blue",
            shadowOffset: {
                width: 0,
                height: 6,
            },
            shadowOpacity: 0.3,
            shadowRadius: 7.5,
            elevation: 12,
            }}>
                <Image source={{uri:this.props.image}} 
                style={{resizeMode:'cover',
                borderRadius:10,
                marginTop: 10,
                height:160,width:100+'%'}} />
                <View style={{flex:1,flexDirection:'row',paddingHorizontal:10,
                    paddingBottom:10,paddingTop:15
                    }}>
                    <Text style={{flex:1,textAlign:'left',fontWeight:'bold'}}>{this.props.name_ru}</Text>
                    <Text style={{flex:1,textAlign:'right'}}>{this.props.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")} сум</Text>
                </View>
                <Text style={{flex:1,paddingHorizontal:10,color:'rgba(0,0,0,.6)',lineHeight:22}}>
                    {this.props.desc_ru}
                </Text>
            </TouchableOpacity>            
        )
    }
}
