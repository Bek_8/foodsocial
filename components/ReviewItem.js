import React from "react";
import { TouchableOpacity, View, StyleSheet, Image, Text } from 'react-native';
import MyContext from "./MyContext"
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class ReviewItem extends React.Component {
    static contextType = MyContext;
    constructor(props){
        super(props);
    }

    render() {
        // let ads = '';
        // ads += this.props.ids.map(item=>' '+item.name);
        return (
            <View style={styles.wrapper} key={`ri`+this.props.id}>
                <View style={styles.details}>
                    <Image source={{uri: this.props.food.image}} style={styles.image}/>
                    <View style={styles.nameBox}>
                        <Text style={styles.name}>{this.props.food.name_ru}{this.props.size.length>0?` (`+this.props.size[0].name+`)`:null}</Text>
                        {this.props.ids.length>0?
                            <Text style={styles.ads}>
                                {/* {ads.trim()} */}
                            </Text> : null}
                        <Text style={styles.price}>{this.props.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")} сум</Text>
                    </View>
                    <Text style={styles.col}>{this.props.amount} шт</Text>
                </View>
            </View>
        );
    }
};


const styles = StyleSheet.create({
    wrapper: {
        margin: 16,
        marginVertical: 0,
        borderBottomWidth: 1,
        borderBottomColor: '#EDEDED'
    },
    image:{
        borderRadius: 25,
        width: 50,
        height: 50,
        marginRight: 16,
    },
    details:{
        padding: 16,
        position: 'relative',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    nameBox:{
        flex: 1,
    },
    distanceBox:{
        flex: 1,
        alignItems: 'flex-end',
    },
    name:{
        marginBottom: 8,
        fontWeight:'bold'
    },
    price:{
        letterSpacing: 1
    },
    col:{
        justifyContent: 'center',
    },
});

