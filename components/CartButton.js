import React, { Component } from 'react'
import { Text, View , StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import MyContext from './MyContext';


export default class CartButton extends Component {
    static contextType = MyContext;
    render() {
        return (
        <MyContext.Provider>
          <View>
            <Icon name="shopping-cart" color={this.props.tintColor} size={30} />
                {this.context.cart.totalItems > 0 ?
                (
                <View style={styles.cartButtonCounter}>
                    <Text style={styles.cartButtonCounterText}>{this.context.cart.totalItems}</Text>
                </View>
                ):
                <View style={styles.cartButtonCounter}>
                    <Text style={styles.cartButtonCounterText}>{this.context.cart.totalItems}</Text>
                </View>
                }        
          </View>
          </MyContext.Provider>
        )
    }
}
const styles = StyleSheet.create({
    menuLabel:{
        textTransform: 'uppercase'
    },
    cartButton:{
        position: 'relative'
    },
    cartButtonCounter:{
        position: 'absolute',
        right: -8,
        top: -2,
        height: 16,
        width: 16,
        borderRadius: 10,
        textAlign: 'center',
        alignItems:'center',
        justifyContent: 'center',
        backgroundColor: '#c22017'
    },
    cartButtonCounterText:{
        textAlign: 'center',
        color: '#ddd',
        fontSize:10
    }
});
