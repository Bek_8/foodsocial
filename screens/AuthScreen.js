import React, { Fragment } from "react";
import { Text, View, Linking, TouchableOpacity, StyleSheet, KeyboardAvoidingView } from 'react-native';
import {Header} from "react-navigation";
import { TextInputMask } from 'react-native-masked-text'
import AsyncStorage from '@react-native-community/async-storage';

export default class AuthScreen extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            isLoggedIn: false,
            user: null,
            redirect: false,
            phone: null,
        };
    }
    static navigationOptions = {
        title: 'Вход',
    };
    onChangeText = (e) => {
        this.setState( {
            phone: e.replace(/[{()}]/g, '').replace(/\s/g, "").replace(/\-/g, "")
        })
    }

    componentDidUpdate(prevProps) {
        if(this.context.token){
            this.props.navigation.navigate('ProfileScreen');
        }else{

        }
    }
    login = () =>{
        fetch("http://165.22.161.165/api/check",{
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                phone: this.state.phone ,
            })
        })
        .then(response => response.json())
        .then(
            (result) => {
                // console.warn(result);
                this.setState({
                isLoggedIn: true,
                user : result
            },
            this.check
            );
        })
        .catch(
            (error) => {
                //console.warn(error)
        });
    }
    check = () => {
        if(this.state.user !== null){
            AsyncStorage.setItem('phone', this.state.phone);
            this.props.navigation.navigate('Verify');
        }
        else{
            //console.warn('Error')
        }
    };

    openLink = (url) => {
        Linking.openURL(url).catch((err) => console.error('An error occurred', err));
    };

    render() {
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.body}>
                <Text style={styles.label}>Мой номер</Text>
                <View style={styles.inputBox}>
                    <Text style={[styles.input,Platform.OS === 'ios'?{marginRight: 12}:{margin: 12}]}>+998</Text>
                <TextInputMask
                    returnKeyType={'done'}
                    keyboardType={'number-pad'} type={'custom'} style={[styles.input , {width:200}]}
                options={{ mask: '(99) 999-99-99'}} value={this.state.phone}
                onChangeText={text => this.onChangeText(text)} />
                </View>
                <TouchableOpacity style={styles.send} onPress={this.state.phone && this.state.phone.length > 7 ? this.login: null}>
                    <Text style={styles.sendText}>Получить код</Text>
                </TouchableOpacity>
            </KeyboardAvoidingView>
        );
    }
}
const styles = StyleSheet.create({
    body: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        position: 'relative',
        backgroundColor: 'white'
    },
    send:{
        height:50,
        paddingVertical:10,
        marginTop:20,
        backgroundColor: 'black'
    },
    sendText:{
        fontSize: 16,
        color: 'white',
        flex: 1,
        textAlign: 'center'
    },
    label:{
        textTransform: 'uppercase',
        fontSize: 16,
        marginVertical: 16,
        letterSpacing: 3
    },
    input:{
        fontSize: 22,
    },
    inputBox:{
        display: 'flex',
        flexDirection: 'row',
        padding: 20,
        borderWidth: 1,
        borderColor: 'blue'},
    bottom:{
        textDecorationLine: 'underline',
        marginTop: 36,
        padding: 24,
    },
    bottomText:{
        textAlign: 'center',
        textDecorationLine: 'underline',
    }
});
