import React, { Fragment} from "react";
import { FlatList, Image, StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';
import axios from "axios";
import MyContext from "../components/MyContext"
import ReviewItem from "../components/ReviewItem"
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class OrderScreen extends React.Component {
    static contextType = MyContext;

    constructor(props){
        super(props);

        this.state = {
            basket: null,
        }
    }
    static navigationOptions = ({ navigation }) => {
        return {
            headerTransparent: true,
            headerLeftContainerStyle:{
                position:'absolute',left:10,top:20,
            },
            headerLeft:(
                <TouchableOpacity activeOpacity={1}
                onPress={() => navigation.goBack()}
                style={{backgroundColor:'rgb(232,51,35)',borderRadius:50,width:50,height:50,
                zIndex:99,
                alignItems:'center'}}>
                    <Icon name="arrow-left" style={{lineHeight:50}} size={20} color={'white'} />                
                </TouchableOpacity>
            )
        }
    };

    componentDidMount() {
        this.getData();
        this._interval = setInterval(this.getData, 5000);
    }

    componentWillUnmount() {
        clearInterval(this._interval);
    }

    getData = () => {
        axios
            .get(`http://165.22.161.165/api/checkout/${this.props.navigation.getParam('id')}`)
            .then(result => {
                    this.setState({
                        basket: result.data
                    });
                },
                (error) => {
                    console.warn(error)
                }
            );
    };

    getPayment = () =>{
        const pay = parseInt(this.state.basket.payment_method);
        const status = parseInt(this.state.basket.is_paid);
        const arr = new Map([
            [0, 'Наличные'],
            [1, 'Payme'],
        ]);
        const sts = new Map([
            [0, 'Не оплачен'],
            [1, 'Оплачен'],
        ]);
        if(pay===1){
            return arr.get(pay)+" ("+sts.get(status)+")";
        }else{
            return arr.get(pay)
        }
    }

    getComment = () => {
        if(this.state.basket.comment){
            return <View>
                <Text>Комментарий:</Text>
                <Text>{this.state.basket.comment}</Text>
            </View>
        }else{
            return null;
        }
    }

    render() {
        if(this.state.basket){
            return (
                <View>
                    <ScrollView style={styles.body}>
                        <View style={{
                            paddingHorizontal:20,
                            paddingTop:20,
                            flexDirection:'row',
                            alingItems:'flex-end',
                            justifyContent:'flex-end',
                            marginBottom:40
                            }}>
                            <Text style={{textTransform:'uppercase',fontSize:20,color:'#ddd',letterSpacing:2,fontWeight:'bold'}}>
                                {'Заказ #'+this.props.navigation.getParam('id')}
                            </Text>
                        </View>                        
                        <View style={styles.details}>
                            <View style={styles.title}>
                                <Text style={styles.titleText}>Текущий статус</Text>
                            </View>
                            <View style={styles.statusBox}>
                                <View style={this.state.basket.status >= 1 ?styles.statusActive : styles.status}>
                                    <Text style={this.state.basket.status >= 1 ? styles.statusTextActive : styles.statusText}>Принят</Text>
                                </View>
                                <View style={this.state.basket.status >= 2 ?styles.statusActive : styles.status}>
                                    <Text style={this.state.basket.status >= 2 ? styles.statusTextActive : styles.statusText}>Обработан</Text>
                                </View>
                                <View style={this.state.basket.status >= 3 ?styles.statusActive : styles.status}>
                                    <Text style={this.state.basket.status >= 3 ? styles.statusTextActive : styles.statusText}>В пути</Text>
                                </View>
                                <View style={this.state.basket.status >= 4 ?styles.statusActive : styles.status}>
                                    <Text style={this.state.basket.status >= 4 ? styles.statusTextActive : styles.statusText}>Доставлено</Text>
                                </View>
                                {this.state.basket.status == -1 ? <Text>Заказ аннулирован</Text> : null}
                            </View>
                        </View>
                        <View style={styles.details}>
                            <View style={styles.title}>
                                <Text style={styles.titleText}>Адрес доставки</Text>
                            </View>
                            <Text style={styles.text}>{this.state.basket.adress}</Text>
                        </View>
                        <View style={styles.items}>
                            {this.state.basket ? this.state.basket.order_food.map(item => (
                                <ReviewItem {...item} key={item.id} />
                            )) : null}
                            <View style={styles.itemsDesc}>
                                <Text style={styles.itemsDescText}>Ресторан:</Text>
                                <Text style={styles.itemsDescPrice}>{this.state.basket.restaurant.name}</Text>
                            </View>
                            <View style={styles.itemsDesc}>
                                <Text style={styles.itemsDescText}>Способ оплаты:</Text>
                                <Text style={styles.itemsDescPrice}>{this.getPayment()}</Text>
                            </View>
                            <View style={styles.itemsDesc}>
                                <Text style={styles.itemsDescText}>Доставка:</Text>
                                <Text style={styles.itemsDescPrice}>Бесплатная</Text>
                            </View>
                            <View style={styles.itemsDesc}>
                                <Text style={styles.itemsDescText}>Стоимость заказа:</Text>
                                <Text style={styles.itemsDescPrice}>{parseInt(this.state.basket.total_price - this.state.basket.shipping_price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")} сум</Text>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            );
        }
        else{
            return this.context.getSpinner();
        }
    }
}

const styles = StyleSheet.create({
    body: {
        height:'100%',
        paddingTop: 8,
        position: 'relative',
        backgroundColor: '#537cbd'
    },    
    details:{
        backgroundColor:'white',
        borderRadius:20,
        padding: 16,
        marginVertical: 8,
        marginHorizontal: 12,
        elevation: 5,
        shadowColor: '#000000',
        shadowOffset: {
            width: 2,
            height: 2
        },
        shadowRadius: 2,
        shadowOpacity: 0.1
    },
    title:{
        paddingBottom: 16,
        marginBottom: 0,
        borderBottomWidth: 1,
        borderBottomColor: '#EDEDED'
    },
    titleText:{
        fontSize: 18,
    },
    statusBox:{
        flexDirection: 'row',
        paddingTop: 16,
    },
    status:{
        flex: 1,
        borderTopWidth: 4,
        paddingTop: 12
    },
    statusText:{
        textAlign: 'center',
        fontSize: 12,
    },
    statusActive:{
        flex: 1,
        borderTopColor: 'lightgreen',
        borderTopWidth: 4,
        paddingTop: 12
    },
    statusTextActive:{
        fontSize: 12,
        fontWeight:'bold'
    },
    text: {
        marginTop: 16,
    },
    items:{
        flex: 1,
        paddingVertical: 16,
        marginBottom: 30,
        marginHorizontal: 10,
        backgroundColor:'white',
        borderRadius:20,
        elevation: 5,
        shadowColor: '#000000',
        shadowOffset: {
            width: 2,
            height: 2
        },
        shadowRadius: 2,
        shadowOpacity: 0.1        
    },
    itemsDesc:{
        marginHorizontal: 16,
        flexDirection: 'row',
        paddingVertical: 8,
        paddingHorizontal: 16
    },
    itemsDescText:{
        fontSize: 14,
        flex: 1,
    },
    itemsDescPrice:{
        flex: 1,
        textAlign: 'right',
        fontSize: 16,
    },
});
